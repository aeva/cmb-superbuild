<p style="text-align:center"><img src="images/CMBLogoSuperBuild.png" alt="CMB SuperBuild Logo" align="middle" style="width: 200px;"/> </p>

# CMB SuperBuild

Though CMB is relatively simple to build itself, it does depend on a several
libraries including Qt, ParaView/VTK, Boost, MOAB, etc.. To help make the process of
building CMB and the libraries it depends on easier we have created a
SuperBuild CMake Project.

# Requirements

* CMake version 3.9 or greater with SSL support (Binaries from `cmake.org`
  have this already; custom built CMake binaries need to use
  `CMAKE_USE_SYSTEM_CURL=ON`).
* ninja or make - (the Windows build requires ninja)
* Checkout of the [CMB SuperBuild Git Repo](https://gitlab.kitware.com/cmb/cmb-superbuild)
* Qt Related Info
  * Have Qt 5.9 or greater (5.12 recommended) installed on the system and make sure you set `USE_SYSTEM_qt5` on
  * In CMake: `QT5_DIR` needs to be set to [Qt installation dir for your compiler]/lib/cmake/Qt5
* C++ Compiler
  * XCode 9.x or greater
  * GCC 4.9 or greater
  * Clang 3.4 or greater
  * Microsoft Visual C++ 2015 / Build Tools 14.0.25123.0 (and possibly later)
* Ubuntu 16.04 specific
  * sudo apt-get install m4
  * sudo apt-get install build-essential
  * turn on `USE_SYSTEM_qt5` in CMake(Recommend)
    * sudo apt-get install qt5-qmake libqt5-dev qt5-dev-tools (if using system qt)
  * sudo apt-get install libxt-dev

Note that the build process will also download additional tarballs and
checkout additional git repos so you will also need an internet connection.

# Building CMB using the SuperBuild Process

## Prepping the Git Repo

1. Clone the CMB SuperBuild Repo using `git clone https://gitlab.kitware.com/cmb/cmb-superbuild.git`
2. Using a shell in the cloned repository check out the latest stable release `git checkout release`
3. Using a shell in the cloned repository, run `git submodule update --init`
4. If you will be changing the superbuild, run `./Scripts/setup-for-development.sh`

## Configuring the Build Using CMake

There are two possible methods you can use: CMake GUI or the ccmake command line tool

### Using the CMake GUI

![](images/CmakeScreenShot.png)

1. Select the Source directory that contains the CMake SuperBuild Git Repo
2. Select the build directory to be used to hold the build.  Note that due to
   a bug in git this should not be under and git repository including the
   Source Directory.

### Using ccmake commandline tool

1. Make the directory you want to build in
2. cd into that directory
3. If you are building with ninja (as oppose to make) run
   `ccmake -G Ninja PathToYourSuperBuildRepo`, else omit the `-G Ninja`

### Configuring the CMB SuperBuild

* By default the build will be in Release Mode (not Debug) - this can be
  changed using the `CMAKE_BUILD_TYPE_cmb` variable. Similar variables exist
  for other projects depending on the setup including `paraview` and `smtk`.
* The process will also build Qt 5.9.1 by default.  If you already have a Qt
  installed (5.9 or newer) then you can do the following:
  * Turn `USE_SYSTEM_qt5` on
  * Tell CMake to configure
  * Check to see if the `Qt5_DIR` variable is set to the appropriate location -
    if is not then set it correctly
  * On Windows, the directory to the Qt5 libraries must be in the `PATH`
    environment variable in order to build.
* Tell CMake to configure
* Tell CMake to generate

## Building the CMB SuperBuild

* cd into the build directory
* run make or ninja - depending on which build system you previously selected.

## Building a CMB Installable Package

* cd into the build directory
* run ctest -R cpack
