if (MSVC AND (MSVC_VERSION LESS 1900) AND pybind11_enabled)
  message(FATAL_ERROR
    "Visual Studio 2015 or later is required to use pybind11.")
endif ()

superbuild_add_project(pybind11
  DEFAULT_ON
  DEPENDS python cxx11
  DEPENDS_OPTIONAL python2 python3
  CMAKE_ARGS
    -DPYBIND11_TEST:BOOL=OFF)
