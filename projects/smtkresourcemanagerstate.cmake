set(smtkresourcemanagerstate_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND smtkresourcemanagerstate_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  smtkresourcemanagerstate_rpaths
  "${smtkresourcemanagerstate_rpaths}")

superbuild_add_project(smtkresourcemanagerstate
  DEBUGGABLE
  DEFAULT_ON
  DEPENDS boost cxx11 libarchive paraview qt5 smtk
  CMAKE_ARGS
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_RPATH:STRING=${smtkresourcemanagerstate_rpaths}
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib)
