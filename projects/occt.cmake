# Build OpenCascade Core Technology
# This is newer than the oce project (which is used by CGM).

superbuild_add_project(occt
  DEPENDS ftgl freetype
  DEPENDS_OPTIONAL cxx11
  CMAKE_ARGS
    -D3RDPARTY_FREETYPE_DIR:PATH=<INSTALL_DIR>
    # Do not build Draw or Visualization modules as they require tcl/tk.
    -DBUILD_MODULE_Draw:BOOL=FALSE
    -DBUILD_MODULE_Visualization:BOOL=FALSE
    # Do not build docs or examples:
    -DBUILD_DOC_Overview:BOOL=OFF
    -DBUILD_SAMPLES_QT:BOOL=OFF
    # Install only what is needed to the superbuild's install dir
    -DINSTALL_DIR:PATH=<INSTALL_DIR>
    -DINSTALL_SAMPLES:BOOL=OFF
    -DINSTALL_TCL:BOOL=OFF
    -DINSTALL_TEST_CASES:BOOL=OFF
    -DINSTALL_TK:BOOL=OFF
)
