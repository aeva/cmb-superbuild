set(vtk_extra_cmake_args)

if (UNIX AND NOT APPLE)
  list(APPEND vtk_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

set(vtkonly_qt_req NO)
if (qt5_enabled)
  set(vtkonly_qt_req YES)
endif ()

superbuild_add_project(vtkonly
  DEBUGGABLE
  DEPENDS
    gdal
    las
    png
    zlib
    netcdf
  DEPENDS_OPTIONAL
    cxx11 freetype python python2 python3 qt5 hdf5
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DVTK_BUILD_TESTING:BOOL=OFF
    -DVTK_WRAP_PYTHON:BOOL=${python_enabled}
    -DVTK_PYTHON_VERSION:STRING=${python_version}
    -DVTK_ENABLE_KITS:BOOL=ON

    -DVTK_MODULE_ENABLE_VTK_GUISupportQt:STRING=${vtkonly_qt_req}
    # CMB needs geovis enabled to provide the gdal reader
    -DVTK_MODULE_ENABLE_VTK_GeovisCore:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_IOGDAL:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_IOLAS:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_IOParallelExodus:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingContextOpenGL2:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingGL2PSOpenGL2:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingMatplotlib:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingQt:STRING=${vtkonly_qt_req}
    -DVTK_MODULE_ENABLE_VTK_TestingRendering:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_ViewsInfovis:STRING=YES

    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_MODULE_USE_EXTERNAL_VTK_freetype:BOOL=${freetype_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_hdf5:BOOL=${hdf5_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_netcdf:BOOL=${netcdf_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_png:BOOL=${png_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_zlib:BOOL=${zlib_enabled}

    -DCMAKE_INSTALL_NAME_DIR:STRING=<INSTALL_DIR>/lib

    ${vtk_extra_cmake_args})

superbuild_add_extra_cmake_args(
  -DVTK_DIR:PATH=<INSTALL_DIR>/lib/cmake/vtk-${vtk_version})
