superbuild_add_project(pyarc
  DEPENDS python
  DEPENDS_OPTIONAL python2 python3
  BUILD_IN_SOURCE 1
  CONFIGURE_COMMAND
  "${CMAKE_COMMAND}"
  -E make_directory
  "<INSTALL_DIR>/lib/python${superbuild_python_version}/site-packages/PyArc"
  BUILD_COMMAND
  <SOURCE_DIR>/scripts/dependencies.sh
  INSTALL_COMMAND
  tar xf "<SOURCE_DIR>/PyARC.tar.gz"
  -C "<INSTALL_DIR>/lib/python${superbuild_python_version}/site-packages/PyArc"
)
