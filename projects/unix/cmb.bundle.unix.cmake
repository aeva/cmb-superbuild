set(library_paths
  "${superbuild_install_location}/lib"
  "${superbuild_install_location}/lib/paraview-${paraview_version}"
  "${superbuild_install_location}/lib/cmb-${cmb_version}")

if (Qt5_DIR)
  list(APPEND library_paths
    "${Qt5_DIR}/../..")
endif ()

# Include libcrypt from the packaging machine.
list(APPEND include_regexes
  ".*/libcrypt.so.*")

if (USE_SYSTEM_qt5 AND Qt5_DIR)
  list(APPEND include_regexes
    ".*/libQt5.*")
endif ()

foreach (executable IN LISTS paraview_executables cmb_programs_to_install)
  superbuild_unix_install_program("${superbuild_install_location}/bin/${executable}"
    "lib"
    SEARCH_DIRECTORIES "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()

foreach (plugin IN LISTS cmb_plugins_paraview)
  superbuild_unix_install_plugin("${plugin}.so"
    "lib"
    "lib/paraview-${paraview_version}/plugins/${plugin}"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/paraview-${paraview_version}/plugins/${plugin}/"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

foreach (plugin IN LISTS cmb_plugins_smtk)
  superbuild_unix_install_plugin("${plugin}.so"
    "lib"
    "lib/smtk-${smtk_version}.${smtk_version_patch}/${plugin}/"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/smtk-${smtk_version}.${smtk_version_patch}/${plugin}/"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

foreach (plugin IN LISTS cmb_plugins_standalone cmb_plugins_cmb)
  superbuild_unix_install_plugin("${plugin}.so"
    "lib"
    "lib/cmb-${cmb_version}/plugins/${plugin}/"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib/cmb-${cmb_version}/plugins/${plugin}/"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

foreach (library IN LISTS cmb_additional_libraries)
  superbuild_unix_install_plugin("lib${plugin}.so"
    "lib"
    "lib"
    SEARCH_DIRECTORIES  "${library_paths}"
    LOCATION            "lib"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes}
  )
endforeach ()

superbuild_unix_install_python(
  LIBDIR              "lib"
  MODULES             ${cmb_python_modules}
  MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
                      "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
  SEARCH_DIRECTORIES  "${library_paths}"
  INCLUDE_REGEXES ${include_regexes}
  EXCLUDE_REGEXES ${exclude_regexes})

if (cmb_install_paraview_python)
  superbuild_unix_install_python(
    LIBDIR              "lib"
    MODULES             paraview vtkmodules
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
                        "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES  "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})

  superbuild_unix_install_python(
    MODULE_DESTINATION  "/site-packages/paraview"
    LIBDIR              "lib"
    MODULES             vtk
    MODULE_DIRECTORIES  "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages"
    SEARCH_DIRECTORIES  "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endif ()

if (pythonrequests_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/requests/cacert.pem"
    DESTINATION "lib/python${superbuild_python_version}/site-packages/requests"
    COMPONENT   superbuild)
endif ()

if (python2_enabled)
  include(python2.functions)
  superbuild_install_superbuild_python2()
elseif (python3_enabled)
  include(python3.functions)
  superbuild_install_superbuild_python3()
endif ()

if (paraviewweb_enabled)
  install(
    FILES       "${superbuild_install_location}/lib/paraview-${paraview_version}/site-packages/paraview/web/defaultProxies.json"
    DESTINATION "lib/python${superbuild_python_version}/site-packages/paraview/web"
    COMPONENT   "${paraview_component}")
  install(
    DIRECTORY   "${superbuild_install_location}/share/paraview/web"
    DESTINATION "share/paraview-${paraview_version}"
    COMPONENT   "${paraview_component}")
endif ()

if (EXISTS "${superbuild_install_location}/bin/paraview.conf")
  install(
    FILES       "${superbuild_install_location}/bin/paraview.conf"
    DESTINATION "bin"
    COMPONENT   "superbuild")
endif ()

if (EXISTS "${superbuild_install_location}/bin/modelbuilder.conf")
  install(
    FILES       "${superbuild_install_location}/bin/modelbuilder.conf"
    DESTINATION "bin"
    COMPONENT   "superbuild")
endif ()

foreach (plugin_file IN LISTS plugin_files)
  get_filename_component(plugin_file_dir "${plugin_file}" DIRECTORY)
  install(
    FILES       "${superbuild_install_location}/${plugin_file}"
    DESTINATION "${plugin_file_dir}"
    COMPONENT   "superbuild")
endforeach ()

set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/cmb.plugins.xml")
cmb_add_plugin("${plugins_file}" ${plugins})

install(
  FILES       "${plugins_file}"
  DESTINATION "lib/cmb-${cmb_version}"
  COMPONENT   superbuild)

install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "share/cmb/workflows"
  COMPONENT   superbuild)

if (qt5_enabled)
  file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/qt.conf" "[Paths]\nPrefix = ..\n")
  install(
    FILES       "${CMAKE_CURRENT_BINARY_DIR}/qt.conf"
    DESTINATION "bin"
    COMPONENT   superbuild)
endif ()

foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
  get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
  get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

  superbuild_unix_install_plugin("${qt5_plugin_path}"
    "lib"
    "plugins/${qt5_plugin_group}/"
    LOADER_PATHS    "${library_paths}"
    INCLUDE_REGEXES ${include_regexes}
    EXCLUDE_REGEXES ${exclude_regexes})
endforeach ()

if (meshkit_enabled)
  foreach (meshkit_exe IN ITEMS coregen assygen)
    superbuild_unix_install_plugin("${superbuild_install_location}/meshkit/bin/${meshkit_exe}"
      "lib"
      "bin"
      SEARCH_DIRECTORIES "${library_paths}")
  endforeach ()
endif ()
