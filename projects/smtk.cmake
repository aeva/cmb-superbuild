include(smtk-version)

set(smtk_extra_cmake_args)
if (WIN32)
  # On Windows we expect the Python source for module to be
  # in a different place than Unix builds and in a different
  # place than SMTK would put it by default. Tell SMTK where
  # to install Python source for the smtk module:
  list(APPEND smtk_extra_cmake_args
    "-DSMTK_PYTHON_MODULEDIR:PATH=bin/Lib/site-packages")
endif ()

set(smtk_enable_python_wrapping)
if (pybind11_enabled)
  set(smtk_enable_python_wrapping ON)
endif ()

set(smtk_enable_vtk OFF)
if (vtkonly_enabled OR paraview_enabled)
  set(smtk_enable_vtk ON)
endif ()

set (enable_smtk_doc always)
if (NOT ENABLE_DOCUMENTATION)
  set (enable_smtk_doc never)
endif()

if (UNIX AND NOT APPLE)
  list(APPEND smtk_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

get_property(smtk_lfs_steps GLOBAL
  PROPERTY cmb_superbuild_lfs_steps)

#explicitly depend on gdal so we inherit the location of the GDAL library
#which FindGDAL.cmake fails to find, even when given GDAL_DIR.
superbuild_add_project(smtk
  DEVELOPER_MODE
  DEBUGGABLE
  DEFAULT_ON
  INDEPENDENT_STEP_TARGETS ${smtk_lfs_steps} download update
  DEPENDS boost cxx11 eigen hdf5 libarchive moab netcdf nlohmannjson pegtl
  DEPENDS_OPTIONAL cumulus gdal netcdf opencv paraview pybind11 python python2
                   python3 matplotlib remus qt5 vtkonly vxl
  CMAKE_ARGS
    # Windows needs help finding the ZeroMQ from the install.
    -DSMTK_RELOCATABLE_INSTALL:BOOL=OFF

    ${smtk_extra_cmake_args}
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DSMTK_RELOCATABLE_INSTALL:BOOL=OFF
    -DBUILD_TESTING:BOOL=${BUILD_TESTING}
    -DSMTK_ENABLE_TESTING:BOOL=${TEST_smtk}
    -DSMTK_BUILD_DOCUMENTATION:STRING=${enable_smtk_doc}
    -DSMTK_ENABLE_OPENCV:BOOL=${opencv_enabled}
    -DSMTK_ENABLE_QT_SUPPORT:BOOL=${qt5_enabled}
    -DSMTK_ENABLE_VTK_SUPPORT:BOOL=${smtk_enable_vtk}
    -DSMTK_ENABLE_VXL_SUPPORT:BOOL=${vxl_enabled}
    -DSMTK_ENABLE_PARAVIEW_SUPPORT:BOOL=${paraview_enabled}
    -DSMTK_ENABLE_DISCRETE_SESSION:BOOL=${smtk_enable_vtk}
    -DSMTK_ENABLE_EXODUS_SESSION:BOOL=${paraview_enabled}
    -DSMTK_ENABLE_REMOTE_SESSION:BOOL=${remus_enabled}
    -DSMTK_ENABLE_REMUS_SUPPORT:BOOL=${remus_enabled}
    -DSMTK_ENABLE_PYTHON_WRAPPING:BOOL=${smtk_enable_python_wrapping}
    -DSMTK_ENABLE_MATPLOTLIB:BOOL=${matplotlib_enabled}
    -DSMTK_USE_PYBIND11:BOOL=${pybind11_enabled}
    # SMTK_USE_SYSTEM_MOAB should go away after CMB's SMTK is bumped to master:
    -DSMTK_USE_SYSTEM_MOAB:BOOL=ON
    -DPYBIND11_INSTALL:BOOL=${pybind11_enabled}
    -DSMTK_QT_VERSION:STRING=5
    -DSMTK_PYTHON_VERSION:STRING=${python_version}

    # This should be off by default because vtkCmbMoabReader in discrete
    # session may only be needed for debugging purpose
    -DSMTK_ENABLE_MOAB_DISCRETE_READER:BOOL=OFF

    # Cumulus bits
    -DSMTK_ENABLE_CUMULUS_SUPPORT:BOOL=${cumulus_enabled}

    # Set CMAKE_INSTALL_LIBDIR to "lib" for all projects in the superbuild to
    # override OS-specific libdirs that GNUInstallDirs.cmake would otherwise
    # set.
    -DCMAKE_INSTALL_LIBDIR:STRING=lib)

set(smtk_config_version ${smtk_version_major}.${smtk_version_minor}.${smtk_version_patch})
if (WIN32)
  set(smtk_config_path "cmake/smtk/${smtk_config_version}")
else ()
  set(smtk_config_path "lib/cmake/smtk/${smtk_config_version}")
endif ()
superbuild_add_extra_cmake_args(-Dsmtk_DIR:PATH=<INSTALL_DIR>/${smtk_config_path})

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()

set(smtk_can_lfs FALSE)
set(smtk_lfs_depends)
if (smtk_SOURCE_SELECTION STREQUAL "git")
  set(smtk_can_lfs TRUE)
elseif (smtk_SOURCE_SELECTION STREQUAL "source")
  if (EXISTS "${smtk_SOURCE_DIR}/.git")
    set(smtk_can_lfs TRUE)
  endif ()
endif ()

option(smtk_FETCH_LFS "Fetch LFS data for CMB" OFF)
if (smtk_enabled AND smtk_can_lfs AND smtk_FETCH_LFS)
  cmb_superbuild_add_lfs_steps(smtk ${smtk_lfs_depends})
endif ()
