# Build and install:
superbuild_add_project(nlohmannjson
  DEPENDS cxx11
  CMAKE_ARGS
    -DBUILD_TESTING:BOOL=OFF
    -DBuildTests:BOOL=OFF
)

# Provide our location to dependent projects:
superbuild_add_extra_cmake_args(
  -Dnlohmann_json_DIR:PATH=<INSTALL_DIR>/lib/cmake/nlohmann_json
)
