set(cmb_extra_optional_dependencies)
if (USE_NONFREE_COMPONENTS)
  list(APPEND cmb_extra_optional_dependencies
    triangle)
endif ()

set(cmb_test_plugin_dir lib)
if (WIN32)
  set(cmb_test_plugin_dir bin)
endif()

set(cmb_extra_cmake_args)
if (UNIX AND NOT APPLE)
  list(APPEND cmb_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

get_property(cmb_lfs_steps GLOBAL
  PROPERTY cmb_superbuild_lfs_steps)

set(cmb_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND cmb_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  cmb_rpaths
  "${cmb_rpaths}")

set(cmb_plugins)
if (rggsession_enabled)
  list(APPEND cmb_plugins
    rgg-session)
endif ()
if (smtkprojectmanager_enabled)
  list(APPEND cmb_plugins
    project-manager)
endif ()
if (smtkresourcemanagerstate_enabled)
  list(APPEND cmb_plugins
    read-and-write-resource-manager-state)
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  cmb_plugins
  "${cmb_plugins}")

superbuild_add_project(cmb
  DEVELOPER_MODE
  DEBUGGABLE
  DEFAULT_ON
  INDEPENDENT_STEP_TARGETS ${cmb_lfs_steps} download update
  DEPENDS boost moab remus vxl nlohmannjson python paraview pybind11
          qt5 smtk zeromq cmbworkflows cmbusersguide smtkusersguide
  DEPENDS_OPTIONAL cumulus ${cmb_extra_optional_dependencies}
                   cxx11 libarchive hdf5 netcdf opencv
                   rggsession
                   smtkprojectmanager
                   smtkresourcemanagerstate
                   python2 python3
  CMAKE_ARGS
    ${cmb_extra_cmake_args}
    -DBUILD_TESTING:BOOL=${BUILD_TESTING}
    -Dcmb_enable_testing:BOOL=${TEST_cmb}

    -DCMB_EXTRA_SMTK_PLUGINS:STRING=${cmb_plugins}

    -DKML_DIR:PATH=<INSTALL_DIR>

    #specify semi-colon separated paths for session plugins
    -Dcmb_test_plugin_paths:STRING=<INSTALL_DIR>/${cmb_test_plugin_dir}

    # specify the apple app install prefix. No harm in specifying it for all
    # platforms.
    -DMACOSX_APP_INSTALL_PREFIX:PATH=<INSTALL_DIR>/Applications

    -DSMTK_ENABLE_TESTING:BOOL=${Test_SMTK}

    # Pass Cumulus flag to smtk
    -DSMTK_ENABLE_CUMULUS_SUPPORT:BOOL=${cumulus_enabled}

    -DCMB_SUPERBUILD_DEVELOPER_ROOT:PATH=<INSTALL_DIR>

    # Set CMAKE_INSTALL_LIBDIR to "lib" for all projects in the superbuild to
    # override OS-specific libdirs that GNUInstallDirs.cmake would otherwise
    # set.
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_RPATH:STRING=${cmb_rpaths})

if ((CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "7.0") OR
    (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND NOT CMAKE_CXX_COMPILER_VERSION VERSION_LESS "3.5"))
 superbuild_append_flags(cxx_flags "-Wno-inconsistent-missing-override" PROJECT_ONLY)
endif ()

set(cmb_can_lfs FALSE)
if (cmb_SOURCE_SELECTION STREQUAL "git")
  set(cmb_can_lfs TRUE)
elseif (cmb_SOURCE_SELECTION STREQUAL "source")
  if (EXISTS "${cmb_SOURCE_DIR}/.git")
    set(cmb_can_lfs TRUE)
  endif ()
endif ()

option(cmb_FETCH_LFS "Fetch LFS data for CMB" OFF)
if (cmb_enabled AND cmb_can_lfs AND cmb_FETCH_LFS)
  cmb_superbuild_add_lfs_steps(cmb)
endif ()
