superbuild_add_project(zeromq
  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=ON
    -DZMQ_BUILD_FRAMEWORK:BOOL=OFF)

superbuild_add_extra_cmake_args(
  -DZeroMQ_ROOT_DIR:PATH=<INSTALL_DIR>)
