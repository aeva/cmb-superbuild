foreach(program IN LISTS cmb_programs_to_install)
  set(additional_libraries)
  set(plugins)
  set(plugin_paths)

  if (program STREQUAL "modelbuilder")
    # Install additional libraries in modelbuilder app
    foreach(library IN LISTS cmb_additional_libraries)
      list(APPEND additional_libraries "${superbuild_install_location}/lib/lib${library}.dylib")
    endforeach()

    foreach (plugin IN LISTS cmb_plugins_paraview)
      list(APPEND plugin_paths "${superbuild_install_location}/lib/paraview-${paraview_version}/plugins/${plugin}/${plugin}.so")
    endforeach ()
    list(APPEND plugins ${cmb_plugins_paraview})

    foreach (plugin IN LISTS cmb_plugins_smtk)
      list(APPEND plugin_paths "${superbuild_install_location}/lib/smtk-${smtk_version}.${smtk_version_patch}/${plugin}/${plugin}.so")
    endforeach ()
    list(APPEND plugins ${cmb_plugins_smtk})

    foreach (plugin IN LISTS cmb_plugins_cmb)
      list(APPEND plugin_paths "${superbuild_install_location}/lib/cmb-${cmb_version}/${plugin}/${plugin}.so")
    endforeach ()
    list(APPEND plugins ${cmb_plugins_cmb})
  endif()

  superbuild_apple_create_app(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    "${superbuild_install_location}/Applications/${program}.app/Contents/MacOS/${program}"
    CLEAN
    PLUGINS ${plugin_paths}
    ADDITIONAL_LIBRARIES ${additional_libraries}
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib")
  foreach (icon_filename MacIcon.icns pvIcon.icns modelbuilder.icns)
    set(icon_path "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${icon_filename}")
    if (EXISTS "${icon_path}")
      install(
        FILES       "${icon_path}"
        DESTINATION "${cmb_package}/${program}.app/Contents/Resources"
        COMPONENT   superbuild)
    endif ()
  endforeach ()
  install(
    FILES       "${superbuild_install_location}/Applications/${program}.app/Contents/Info.plist"
    DESTINATION "${cmb_package}/${program}.app/Contents"
    COMPONENT   superbuild)

  if (EXISTS "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${program}.conf")
    file(READ "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${program}.conf" conf_contents)
    string(REGEX REPLACE "[^\n]*/" "../Plugins/" pkg_conf_contents "${conf_contents}")
    file(WRITE "${CMAKE_CURRENT_BINARY_DIR}/${program}.conf" "${pkg_conf_contents}")
    install(
      FILES       "${CMAKE_CURRENT_BINARY_DIR}/${program}.conf"
      DESTINATION "${cmb_package}/${program}.app/Contents/Resources/"
      COMPONENT   superbuild)

    string(REPLACE "\n" ";" plugin_files "${conf_contents}")
    foreach (plugin_file IN LISTS plugin_files)
      if (NOT plugin_file)
        continue ()
      endif ()
      if (IS_ABSOLUTE "${plugin_file}")
        set(plugin_source_path "${plugin_file}")
      else ()
        set(plugin_source_path "${superbuild_install_location}/Applications/${program}.app/Contents/Resources/${plugin_file}")
      endif ()
      install(
        FILES       "${plugin_source_path}"
        DESTINATION "${cmb_package}/${program}.app/Contents/Plugins/"
        COMPONENT   superbuild)
    endforeach ()
  endif ()

  foreach (executable IN LISTS paraview_executables)
    superbuild_apple_install_utility(
      "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
      "${program}.app"
      "${superbuild_install_location}/bin/${executable}"
      SEARCH_DIRECTORIES
              "${superbuild_install_location}/lib")
  endforeach ()

  install(CODE
    "file(MAKE_DIRECTORY \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources\")
    file(WRITE \"\${CMAKE_INSTALL_PREFIX}/${cmb_package}/${program}.app/Contents/Resources/qt.conf\" \"\")"
    COMPONENT superbuild)

  set(plugins_file "${CMAKE_CURRENT_BINARY_DIR}/${program}.plugins")
  cmb_add_plugin("${plugins_file}" ${plugins})

  superbuild_apple_install_python(
    "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
    "${program}.app"
    MODULES ${cmb_python_modules}
    MODULE_DIRECTORIES
            "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages"
            "${superbuild_install_location}/Applications/paraview.app/Contents/Python"
    SEARCH_DIRECTORIES
            "${superbuild_install_location}/lib"
            "${superbuild_install_location}/Applications/paraview.app/Contents/Libraries")

  if (matplotlib_enabled)
    install(
      DIRECTORY   "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/matplotlib/mpl-data/"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/matplotlib/mpl-data"
      COMPONENT   superbuild)
  endif ()

  if (pythonrequests_enabled)
    install(
      FILES       "${superbuild_install_location}/lib/python${superbuild_python_version}/site-packages/requests/cacert.pem"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/requests"
      COMPONENT   superbuild)
  endif ()

  if (paraviewweb_enabled)
    install(
      FILES       "${superbuild_install_location}/Applications/paraview.app/Contents/Python/paraview/web/defaultProxies.json"
      DESTINATION "${cmb_package}/${program}.app/Contents/Python/paraview/web"
      COMPONENT   "superbuild")
    install(
      DIRECTORY   "${superbuild_install_location}/share/paraview/web"
      DESTINATION "${cmb_package}/${program}.app/Contents/Resources"
      COMPONENT   "superbuild")
  endif ()

  foreach (qt5_plugin_path IN LISTS qt5_plugin_paths)
    get_filename_component(qt5_plugin_group "${qt5_plugin_path}" DIRECTORY)
    get_filename_component(qt5_plugin_group "${qt5_plugin_group}" NAME)

    superbuild_apple_install_module(
      "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
      "${program}.app"
      "${qt5_plugin_path}"
      "Contents/Plugins/${qt5_plugin_group}"
      SEARCH_DIRECTORIES  "${library_paths}")
  endforeach ()

  if (cmb_doc_base_dir)
    set(cmb_doc_dir "${cmb_package}/${program}.app/${cmb_doc_base_dir}")
  endif ()

  # Install PDF guides.
  cmb_install_extra_data()
endforeach ()

# FIXME: Install inside of each application?
install(
  DIRECTORY   "${superbuild_install_location}/share/cmb/workflows/"
  DESTINATION "Workflows"
  COMPONENT   superbuild)

if (meshkit_enabled)
  foreach (meshkit_exe IN ITEMS coregen assygen)
    superbuild_apple_install_utility(
      "\${CMAKE_INSTALL_PREFIX}/${cmb_package}"
      "modelbuilder.app"
      "${superbuild_install_location}/bin/${meshkit_exe}"
      SEARCH_DIRECTORIES  "${library_paths}"
      FRAMEWORK_DEST      "Frameworks/meshkit"
      LIBRARY_DEST        "Libraries/meshkit")
  endforeach ()
endif ()
