# The common superbuild's hdf5 configuration does not set CMAKE_INSTALL_RPATH,
# so we override it with our own hdf5 configuration.

superbuild_add_project(hdf5
  CAN_USE_SYSTEM
  DEPENDS zlib szip

  CMAKE_ARGS
    -DBUILD_SHARED_LIBS:BOOL=${BUILD_SHARED_LIBS}
    -DCMAKE_INSTALL_RPATH:PATH=$ORIGIN/../lib
    -DHDF5_ENABLE_Z_LIB_SUPPORT:BOOL=TRUE
    -DHDF5_ENABLE_SZIP_SUPPORT:BOOL=TRUE
    -DHDF5_ENABLE_SZIP_ENCODING:BOOL=TRUE
    -DHDF5_BUILD_HL_LIB:BOOL=TRUE
    -DHDF5_BUILD_WITH_INSTALL_NAME:BOOL=ON)

superbuild_add_extra_cmake_args(
  -DHDF5_ROOT:PATH=<INSTALL_DIR>
  -DHDF5_NO_FIND_PACKAGE_CONFIG_FILE:BOOL=ON)

superbuild_apply_patch(hdf5 fix-ext-pkg-find
  "Force proper logic for zlib and szip dependencies")
