# Consolidates platform independent stub for cmb.bundle.cmake files.

include(cmb-version)
include(paraview-version)
include(smtk-version)

set(CPACK_PACKAGE_VENDOR "Kitware, Inc.")
set(CPACK_PACKAGE_VERSION_MAJOR ${cmb_version_major})
set(CPACK_PACKAGE_VERSION_MINOR ${cmb_version_minor})
set(CPACK_PACKAGE_VERSION_PATCH ${cmb_version_patch}${cmb_version_suffix})
if (CMB_PACKAGE_SUFFIX)
  set(CPACK_PACKAGE_VERSION_PATCH ${CPACK_PACKAGE_VERSION_PATCH}-${CMB_PACKAGE_SUFFIX})
endif ()

if (NOT DEFINED package_filename)
  set(package_filename "${CMB_PACKAGE_FILE_NAME}")
endif ()

if (package_filename)
  set(CPACK_PACKAGE_FILE_NAME "${package_filename}")
else ()
  set(CPACK_PACKAGE_FILE_NAME
    "${cmb_package_name}-${CPACK_PACKAGE_VERSION_MAJOR}.${CPACK_PACKAGE_VERSION_MINOR}.${CPACK_PACKAGE_VERSION_PATCH}")
endif ()

# Set the license file.
set(CPACK_RESOURCE_FILE_LICENSE
    "${superbuild_source_directory}/License.txt")

list(SORT cmb_programs_to_install)
list(REMOVE_DUPLICATES cmb_programs_to_install)

function (cmb_add_plugin output)
  set(contents "<?xml version=\"1.0\"?>\n<Plugins>\n</Plugins>\n")
  foreach (name IN LISTS ARGN)
    set(plugin_directive "  <Plugin name=\"${name}\" auto_load=\"0\" />\n")
    string(REPLACE "</Plugins>" "${plugin_directive}</Plugins>" contents "${contents}")
  endforeach ()
  file(WRITE "${output}" "${contents}")
endfunction ()

set(paraview_executables)
if (cmb_install_paraview_server)
  list(APPEND paraview_executables
    pvserver
    pvdataserver
    pvrenderserver)
endif ()
if (cmb_install_paraview_python)
  list(APPEND paraview_executables
    pvbatch
    pvpython)
endif ()

function (extract_plugin_list varname pluginxml)
  file(STRINGS "${pluginxml}"
    plugin_lines
    REGEX "name=\"[A-Za-z0-9]+\"")
  set(plugin_names)
  foreach (plugin_line IN LISTS plugin_lines)
    string(REGEX REPLACE ".*name=\"\([A-Za-z0-9]+\)\".*" "\\1" plugin_name "${plugin_line}")
    list(APPEND plugin_names
      "${plugin_name}")
  endforeach ()
  set("${varname}"
    ${plugin_names}
    PARENT_SCOPE)
endfunction ()

set(paraview_plugin_files
  "${plugin_dir}/paraview-${paraview_version}/plugins/paraview.plugins.xml")

set(cmb_plugins_paraview)
foreach (plugin_file IN LISTS paraview_plugin_files)
  extract_plugin_list(plugins "${superbuild_install_location}/${plugin_file}")
  list(APPEND cmb_plugins_paraview
    ${plugins})
endforeach ()

set(smtk_plugin_files
  "${plugin_dir}/smtk-${smtk_version}.${smtk_version_patch}/smtk.extensions.xml"
  "${plugin_dir}/smtk-${smtk_version}.${smtk_version_patch}/smtk.paraview.xml")

if (smtkprojectmanager_enabled)
  list(APPEND smtk_plugin_files
    "${plugin_dir}/smtk-${smtk_version}.${smtk_version_patch}/smtk.projectmanager.xml")
endif ()
if (smtkresourcemanagerstate_enabled)
  list(APPEND smtk_plugin_files
    "${plugin_dir}/smtk-${smtk_version}.${smtk_version_patch}/smtk.readwriteresourcemanagerstate.xml")
endif ()
if (rggsession_enabled)
  list(APPEND smtk_plugin_files
    "${plugin_dir}/smtk-${smtk_version}.${smtk_version_patch}/smtk.rggsession.xml")
endif ()

set(cmb_plugins_smtk)
foreach (plugin_file IN LISTS smtk_plugin_files)
  extract_plugin_list(plugins "${superbuild_install_location}/${plugin_file}")
  list(APPEND cmb_plugins_smtk
    ${plugins})
endforeach ()

set(cmb_plugin_files
  "${plugin_dir}/cmb-${cmb_version}/cmb.xml")

foreach (plugin_file IN LISTS cmb_plugin_files)
  extract_plugin_list(plugins "${superbuild_install_location}/${plugin_file}")
  list(APPEND cmb_plugins_cmb
    ${plugins})
endforeach ()

list(REMOVE_ITEM cmb_plugins_cmb
  # This is a static plugin.
  cmbPostProcessingModePlugin)

list(APPEND plugin_files
  ${paraview_plugin_files}
  ${smtk_plugin_files}
  ${cmb_plugin_files})

set(cmb_additional_libraries)
foreach (boost_lib IN LISTS BOOST_ADDITIONAL_LIBRARIES)
  list(APPEND cmb_additional_libraries boost_${boost_lib})
endforeach ()

set(cmb_python_modules
  smtk
  paraview
  cinema_python
  pygments
  six
  vtk
  vtkmodules)

if (matplotlib_enabled)
  list(APPEND cmb_python_modules
    matplotlib)
endif ()

if (numpy_enabled)
  list(APPEND cmb_python_modules
    numpy)
endif ()

if (pythongirderclient_enabled)
  list(APPEND cmb_python_modules
    diskcache
    requests
    requests_toolbelt
    girder_client)
endif ()

if (paraviewweb_enabled)
  list(APPEND cmb_python_modules
    autobahn
    constantly
    incremental
    twisted
    wslink
    zope)

  if (WIN32)
    list(APPEND cmb_python_modules
      adodbapi
      isapi
      pythoncom
      win32com)
  endif ()
endif ()

if (qt5_enabled)
  include(qt5.functions)

  set(qt5_plugin_prefix)
  if (NOT WIN32)
    set(qt5_plugin_prefix "lib")
  endif ()

  # Add SVG support, so cmb can use paraview SVG icons.
  set(qt5_plugins
    iconengines/${qt5_plugin_prefix}qsvgicon
    imageformats/${qt5_plugin_prefix}qsvg
    sqldrivers/${qt5_plugin_prefix}qsqlite)

  if (WIN32)
    list(APPEND qt5_plugins
      platforms/qwindows)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/qwindowsvistastyle)
    endif ()
  elseif (APPLE)
    list(APPEND qt5_plugins
      platforms/libqcocoa
      printsupport/libcocoaprintersupport)

    if (NOT qt5_version VERSION_LESS "5.10")
      list(APPEND qt5_plugins
        styles/libqmacstyle)
    endif ()
  elseif (UNIX)
    list(APPEND qt5_plugins
      platforms/libqxcb
      platforminputcontexts/libcomposeplatforminputcontextplugin
      xcbglintegrations/libqxcb-glx-integration)
  endif ()

  superbuild_install_qt5_plugin_paths(qt5_plugin_paths ${qt5_plugins})
else ()
  set(qt5_plugin_paths)
endif ()

function (cmb_install_pdf project filename)
  if (${project}_enabled)
    install(
      FILES       "${superbuild_install_location}/doc/${filename}"
      DESTINATION "${cmb_doc_dir}"
      COMPONENT   superbuild)
  endif ()
endfunction ()

function (cmb_install_extra_data)
  if (cmb_doc_dir)
    cmb_install_pdf(cmbusersguide "CMBUsersGuide.pdf")
    cmb_install_pdf(smtkusersguide "SMTKUsersGuide.pdf")
  endif ()
endfunction ()
