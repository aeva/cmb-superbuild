superbuild_add_project(itk
  DEPENDS cxx11
  CMAKE_ARGS
    # Build what you need
    -DBUILD_EXAMPLES:BOOL=OFF
    -DBUILD_TESTING:BOOL=OFF
    -DITK_BUILD_DEFAULT_MODULES:BOOL=ON
    -DITK_BUILD_DOCUMENTATION:BOOL=OFF
    -DITK_WRAP_PYTHON:BOOL=OFF
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
)

# TODO: Fix this when ITK allows ITKVtkGlue to be built against
#       an installed ITK instead of the build tree. It should be:
#       <INSTALL_DIR>/lib/cmake/ITK-5.1
set(itk_itk_dir ${CMAKE_CURRENT_BINARY_DIR}/itk/build)

superbuild_add_extra_cmake_args(
  -DITK_DIR:PATH=${itk_itk_dir})
