superbuild_add_project(itkvtkglue
  DEPENDS cxx11 itk paraview qt5 boost
  CMAKE_ARGS
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    # find_package(Boost) gets confused without this:
    -DBoost_USE_STATIC_LIBS:BOOL=OFF
)

## The ITKVtkGlue module uses the same configuration directory
## as ITK itself does, so there is no need to add it explicitly:
# superbuild_add_extra_cmake_args(
#   -DITKVTKGLUE_DIR:PATH=<INSTALL_DIR>/lib/cmake/ITK-5.1)
