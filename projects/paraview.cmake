set(paraview_extra_cmake_args)
if (PV_NIGHTLY_SUFFIX)
  list(APPEND paraview_extra_cmake_args
    -DPV_NIGHTLY_SUFFIX:STRING=${PV_NIGHTLY_SUFFIX})
endif ()

if (UNIX AND NOT APPLE)
  list(APPEND paraview_extra_cmake_args
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=FALSE)
endif ()

set(paraview_rpaths)
if (APPLE AND USE_SYSTEM_qt5)
  # On macOS, Qt5 packages use `@rpath` as their library ids. Add an rpath for
  # it to the build.
  list(APPEND paraview_rpaths
    "${qt5_rpath}")
endif ()
string(REPLACE ";" "${_superbuild_list_separator}"
  paraview_rpaths
  "${paraview_rpaths}")

# Ubuntu toolchains don't allow undefined symbols, but VTK's autodetection
# doesn't see this.
if (EXISTS "/etc/os-release")
  file(STRINGS "/etc/os-release" paraview_os_id REGEX "^ID=")
  if (paraview_os_id MATCHES "ubuntu")
    list(APPEND paraview_extra_cmake_args
      -Dvtk_undefined_symbols_allowed:BOOL=OFF)
  endif ()
endif ()

superbuild_add_project(paraview
  DEBUGGABLE
  DEPENDS
    boost
    freetype
    gdal
    las
    png
    python
    qt5
    zlib
    netcdf
  DEPENDS_OPTIONAL
    cxx11 hdf5 paraviewweb protobuf python2 python3
  CMAKE_ARGS
    -DPARAVIEW_BUILD_SHARED_LIBS:BOOL=ON
    -DPARAVIEW_BUILD_TESTING:BOOL=OFF
    -DPARAVIEW_PLUGIN_ENABLE_SLACTools:BOOL=ON
    -DPARAVIEW_PLUGINS_DEFAULT:BOOL=OFF
    -DPARAVIEW_BUILD_QT_GUI:BOOL=${qt5_enabled}
    -DPARAVIEW_ENABLE_PYTHON:BOOL=${python_enabled}
    -DPARAVIEW_ENABLE_WEB:BOOL=OFF
    -DPARAVIEW_USE_MPI:BOOL=${mpi_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_hdf5:BOOL=${hdf5_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_netcdf:BOOL=${netcdf_enabled}
    -DVTK_MODULE_USE_EXTERNAL_ParaView_protobuf:BOOL=${protobuf_enabled}
    -DPARAVIEW_ENABLE_KITS:BOOL=ON
    -DPARAVIEW_USE_ICE_T:BOOL=ON
    -DCMAKE_INSTALL_LIBDIR:STRING=lib
    -DCMAKE_INSTALL_NAME_DIR:PATH=<INSTALL_DIR>/lib
    -DPARAVIEW_PYTHON_VERSION:STRING=${python_version}

    #currently catalyst is having problems on praxis so lets disable it for now
    -DPARAVIEW_ENABLE_CATALYST:BOOL=OFF

    # enable VisIt bridge for additional file readers
    -DPARAVIEW_ENABLE_VISITBRIDGE:BOOL=ON

    #CMB needs geovis enabled to provide the gdal reader
    -DVTK_MODULE_ENABLE_VTK_GeovisCore:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_ViewsInfovis:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingMatplotlib:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_RenderingGL2PSOpenGL2:STRING=YES
    -DVTK_MODULE_ENABLE_VTK_DomainsChemistryOpenGL2:STRING=YES
    -DPARAVIEW_ENABLE_GDAL:BOOL=ON
    -DPARAVIEW_ENABLE_LAS:BOOL=ON

    # CMB needs to specify external plugins so that we can let paraview
    # properly install the plugins. So we sneakily force a variable that is an
    # implementation detail of paraview branding
    -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=TRUE
    # since VTK mangles all the following, I wonder if there's any point in
    # making it use system versions.
    -DVTK_MODULE_USE_EXTERNAL_VTK_freetype:BOOL=${freetype_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_png:BOOL=${png_enabled}
    -DVTK_MODULE_USE_EXTERNAL_VTK_zlib:BOOL=${zlib_enabled}

    #If this is true paraview doesn't properly clean the paths to system
    #libraries like netcdf
    -DCMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=TRUE
    -DCMAKE_MACOSX_RPATH:BOOL=FALSE
    -DCMAKE_INSTALL_RPATH:STRING=${paraview_rpaths}

    ${paraview_response_file}

    # Keep up with the latest ParaView & VTK by avoiding the use of legacy API
    -DVTK_LEGACY_REMOVE:BOOL=TRUE

    ${paraview_extra_cmake_args})

set(paraview_paraview_dir "<INSTALL_DIR>/lib/cmake/paraview-${paraview_version}")

superbuild_add_extra_cmake_args(
  -DParaView_DIR:PATH=${paraview_paraview_dir}
  -DParaView_CLEXECUTABLES_DIR:PATH=<INSTALL_DIR>/bin
  -DVTK_DIR:PATH=${paraview_paraview_dir}/vtk)
