set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
  "CMB modelbuilder application")
set(CPACK_PACKAGE_NAME "CMB")
set(cmb_package_name "modelbuilder")

set(cmb_programs_to_install
  modelbuilder
  )

set(cmb_install_paraview_server FALSE)
set(cmb_install_paraview_python TRUE)

include(cmb.bundle.common)
