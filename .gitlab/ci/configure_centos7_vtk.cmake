# Configure the vtkonly build.
set(ENABLE_cmb "OFF" CACHE BOOL "")
set(ENABLE_paraview "OFF" CACHE BOOL "")
set(ENABLE_smtkprojectmanager "OFF" CACHE BOOL "")
set(ENABLE_smtkresourcemanagerstate "OFF" CACHE BOOL "")
set(ENABLE_vtkonly "ON" CACHE BOOL "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_centos7.cmake")
