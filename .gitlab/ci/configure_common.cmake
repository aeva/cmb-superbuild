set(CTEST_USE_LAUNCHERS "ON" CACHE STRING "")

# Enable SMTK testing.
set(TEST_smtk "ON" CACHE BOOL "")
set(smtk_FETCH_LFS "ON" CACHE BOOL "")

# Disable CMB testing.
set(TEST_cmb "OFF" CACHE BOOL "")

# Suppress ParaView and Qt5 output. It is too large for the log to follow.
set(SUPPRESS_paraview_OUTPUT "ON" CACHE BOOL "")
set(SUPPRESS_qt5_OUTPUT "ON" CACHE BOOL "")

include("${CMAKE_CURRENT_LIST_DIR}/configure_sccache.cmake")
