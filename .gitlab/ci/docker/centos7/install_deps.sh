#!/bin/sh

# Install build requirements.
yum install -y \
    zlib-devel libcurl-devel python-devel \
    freeglut-devel glew-devel graphviz-devel libpng-devel \
    libxcb libxcb-devel libXt-devel xcb-util xcb-util-devel mesa-libGL-devel \
    libxkbcommon-devel file mesa-dri-drivers

# Install EPEL
yum install -y \
    epel-release

# Install development tools
yum install -y \
    cmake3 \
    git-core \
    git-lfs \
    ninja-build

# Install toolchains.
yum install -y \
    centos-release-scl
yum install -y \
    devtoolset-7-gcc-c++ \
    devtoolset-7 \
    devtoolset-7-gcc

yum clean all
