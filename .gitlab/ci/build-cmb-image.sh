#!/bin/sh

set -e
set -x

# Do not push images for builds from not-cmb-superbuild.
if [ "$CI_PROJECT_PATH" = "cmb/cmb-superbuild" ]; then
    push_image=true
else
    push_image=false
fi
readonly push_image

# Determine the CMB branch to look at.
if [ "$CI_COMMIT_REF_NAME" = "release" ]; then
    cmb_tag=release
else
    # As good a guess as any.
    cmb_tag=master
fi
readonly cmb_tag

readonly ci_image_tag="$1"
shift

readonly ci_image_name="$1"
shift

readonly date="$( date "+%Y%m%d" )"
readonly image_tag_date="ci-cmb-$ci_image_tag-$date"
readonly image_tag_latest="ci-cmb-$ci_image_tag-latest"

# Use podman to avoid having to do docker-in-docker shenanigans.
readonly docker="podman --storage-driver=vfs"

# Install the tools we'll need.
dnf install -y git-core podman-docker
# Clone CMB for its image scripts.
git clone --depth 1 https://gitlab.kitware.com/cmb/cmb.git -b "$cmb_tag" .cmb
# Pull the existing image (if available).
$docker pull "kitware/cmb:$image_tag_latest" || :
# Build the new image.
$docker build --format=docker \
    --volume "$PWD/.gitlab:/root/helpers:Z" \
    "--build-arg=SCCACHE_REDIS=$SCCACHE_REDIS" \
    "--build-arg=superbuild_ref=$CI_COMMIT_SHA" \
    -t "kitware/cmb:$image_tag_date" \
    ".cmb/.gitlab/ci/docker/$ci_image_name" \
    >build.log
# Tag it as the latest.
$docker tag "kitware/cmb:$image_tag_date" "kitware/cmb:$image_tag_latest"

if $push_image; then
    # Push the images to DockerHub.
    $docker login --username "$DOCKERHUB_USERNAME" --password "$DOCKERHUB_PASSWORD" "https://index.docker.io/v1/"
    $docker push "kitware/cmb:$image_tag_date"
    $docker push "kitware/cmb:$image_tag_latest"
fi
